

var a = "texto1"
var b = "texto2" ;
var $c = "texto3'hola'con comillas simples dentro"


var x,y 
x = 5;
y = 10;

x++;
x = x + 1;
x--;

//document.write('la suma:' + (x + y));
//document.write('<hr/>');
//document.write('la multiplicacion:' + (x * y));

var h = true;
var z = false;

console.log(h && z); //AND
console.log(h || z); //OR
console.log(h == z);
console.log(h != z);

//tipo array

var contactos;

contactos = ["pepe","paco", 33, true, "antonio"];

console.log(contactos[0]);
console.log(contactos[1]);
console.log(contactos.length);


//objeto

var usuario = {

    nombre:"pepe",
    apellidos:"Lotas",
    email:"pepe@lotas.es",
    lenguajes:["html", "css", "php"]
}


document.write(usuario.nombre);
document.write("<hr/>");
document.write(usuario.apellidos);
document.write("<hr/>");
document.write(usuario.email);

