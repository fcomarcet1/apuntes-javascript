var hola = "Hola mundo"

console.log(hola);
console.log("mensaje stándard por la consola");
console.log("%cMensaje con la tipografía modificada  y el color verde", "color: green; font-family:'Comic Sans MS'");
console.log("%cMensaje con la tipografía modificada Impact y el color naranja", "color: orange; font-family:Impact");

//limpiar consola
console.clear()

document.write(hola);

alert(hola);